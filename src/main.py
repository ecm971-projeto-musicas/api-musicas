from fastapi import FastAPI, HTTPException
from src.domain.usecases.get_all_musics import GetAllMusics
from src.domain.usecases.rate_music import RateMusic
from src.domain.usecases.save_music import SaveMusic
from src.domain.entities.music import Music

import os
import sys

root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(root_dir)

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Olá, mundo!"}

@app.get("/ping")
async def ping():
    return "pong"

@app.post("/music")
async def post_music(name: str):
    music = SaveMusic.call(name)
    return music

@app.get("/music")
async def get_all_musics():
    musics = GetAllMusics.call()
    return musics

@app.post("/rate")
async def rate_music(name: str, rating: int):
    try:
        music = RateMusic.call(name, rating)
        return music
    except Exception as e:
        raise HTTPException(status_code=400, detail=e.args[0])

    

