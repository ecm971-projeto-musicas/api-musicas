from src.infra.repositories.music_repository import MusicRepository
from src.domain.entities.music import Music

class RateMusic:
    def call(name: str, rating: int) -> Music:
        
        if(rating<1 or rating>5):
            raise Exception("Avaliacao deve ser um numero entra 1 e 5")

        repository = MusicRepository()
        music = repository.update_music(name, rating)
        return music