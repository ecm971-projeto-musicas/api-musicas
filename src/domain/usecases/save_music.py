from src.infra.repositories.music_repository import MusicRepository
from src.domain.entities.music import Music

class SaveMusic:
    def call(name: str) -> Music:
        repository = MusicRepository()
        music = repository.insert_music(name)
        return music
        