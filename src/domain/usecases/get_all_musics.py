from src.infra.repositories.music_repository import MusicRepository
from src.domain.entities.music import Music

class GetAllMusics:
    def call() -> list[Music]:
        repository = MusicRepository()
        musics = repository.get_all_musics()
        return musics