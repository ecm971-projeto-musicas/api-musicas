import sqlite3
from src.domain.entities.music import Music

class MusicRepository:

    def __init__(self) -> None:
        pass

    def get_all_musics(self) -> list[Music]:
        conn = sqlite3.connect('music.db')
        cursor = conn.cursor()

        cursor.execute('SELECT * FROM music ORDER BY rating DESC, name')
        rows = cursor.fetchall()

        music_list = []
        for row in rows:
            id, name, rating = row
            music = Music(id=id, name=name, rating=rating)
            music_list.append(music)

        conn.close()

        return music_list

    def insert_music(self, name: str) -> Music:
        conn = sqlite3.connect('music.db')
        cursor = conn.cursor()

        cursor.execute('INSERT INTO music (name, rating) VALUES (?, 0)', (name,))
        conn.commit()

        music_id = cursor.lastrowid

        conn.close()

        return Music(music_id, name, 0)

    def update_music(self, name: str, rating: int) -> Music:
        conn = sqlite3.connect('music.db')
        cursor = conn.cursor()

        cursor.execute('UPDATE music SET rating = ? WHERE name = ?', (rating, name))
        conn.commit()

        cursor.execute('SELECT * FROM music WHERE name = ? ORDER BY name, rating', (name,))
        rows = cursor.fetchall()

        music_list = []
        for row in rows:
            id, name, rating = row
            music = Music(id=id, name=name, rating=rating)
            music_list.append(music)

        conn.close()

        return music_list[0]
