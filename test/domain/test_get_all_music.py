from src.domain.usecases.get_all_musics import GetAllMusics

def test_get_all_music_ordened():
    musics = GetAllMusics.call()

    # Order by desc rating and name
    sorted_musics = sorted(musics, key=lambda music: (-music.rating, music.name))

    assert musics == sorted_musics