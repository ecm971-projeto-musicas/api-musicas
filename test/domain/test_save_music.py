from datetime import datetime
from src.domain.usecases.get_all_musics import GetAllMusics
from src.domain.usecases.save_music import SaveMusic

def test_save_music():

    music_test_name = f'test_music{datetime.now().timestamp()}'
    saved_music = SaveMusic.call(music_test_name)   
    assert saved_music.rating == 0